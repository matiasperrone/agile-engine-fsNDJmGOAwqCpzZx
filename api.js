const express = require('express')
const serveStatic = require('serve-static')
const path = require('path')
const app = express()
const hostname = 'localhost';
const port = 3000;
let info = {
  balance: 0,
  transactions: [],
  lock: false,
};


app.use(express.json())
app.use(serveStatic('public/ftp', { 'index': ['index.html'] }))
app.use(serveStatic(path.join(__dirname, 'dist')))
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
})

// Balance
app.get('/api', (req, res) => {
  while (info.lock) { sleep(10) }
  const msg = { balance: info.balance }
  const status = 200
  res.status(status).send(msg)
})

// Transaction
app.get('/api/transactions', (req, res) => {
  while (info.lock) { sleep(10) }

  const msg = info.transactions
  const status = 200
  res.status(status).send(msg)
})

app.get('/api/transactions/:id', (req, res) => {
  while (info.lock) { sleep(10) }
  let msg = null
  let status = 404
  const ix = parseInt(req.params.id)
  if (!isNaN(ix)) {
    if (info.transactions[ix]) {
      msg = info.transactions[ix]
      status = 200
    }
  }
  res.status(status).send(msg)
})
app.post('/api/transactions', (req, res) => {
  while (info.lock) { sleep(10) }
  let msg = null
  let status = 404
  info.lock = true

  try {
    const currentBalance = info.balance
    const transactionLength = info.transactions.length
    let transaction = req.body
    const mult = (transaction.type === 'debit' ? -1 : transaction.type === 'credit' ? 1 : 0)

    transaction.amount = parseFloat(transaction.amount);
    if (!isNaN(transaction.amount))
    {
      const newBalance = info.balance + transaction.amount * mult
      if (newBalance >= 0)
      {
        info.balance = newBalance
        status = 201
        transaction.id = info.transactions.length
        transaction.effectiveDate = new Date
        info.transactions.push(transaction)
        msg = transaction
      }
      else
      {
        status = 412
        msg = {error: "Insuficient funds"}
      }
    }
    else
    {
      status = 400
    }
  }
  catch (e)
  {
    status = 500
    msg = { error: e.message }

    info.balance = currentBalance
    if (info.transactions.length > transactionLength)
    {
      delete(info.transactions[transactionLength])
    }
  }
  finally {
    info.lock = false;
  }
  res.status(status).send(msg)
} )
