## Errors list: (HTTP Codes)

- 400 "Bad Request": When a parameter is missing or has wrong data
- 404 "Not Found": When the account is different from the one informed
- 412 "Precondition Failed": Insuficient funds

## Run
node api.js
